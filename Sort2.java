import java.util.Arrays;

public class Sort2 {


   // public class Sorting {
        public static void sortStringArr(String[] arr) {

            boolean s = false;
            while (!s) {
                s = true;
                for (int i = 0; i < arr.length - 1; i++) {
                    if (arr[i].compareTo(arr[i + 1]) > 0) {
                        s = false;
                        String bat;
                        bat = arr[i];
                        arr[i] = arr[i + 1];
                        arr[i + 1] = bat;

                    }
                }

            }

        }

        public static void sortStringArr2(String[] arr) {

            String temp;
            for (int i = 0; i <= arr.length; i++){

                String smallest = arr[i];


                for (int j = 0; j <= arr.length; j++){
                    if (arr[j].compareTo(arr[i]) > 0){
                        temp = smallest;
                        arr[j] = temp;
                        arr[i] = smallest;
                    }
                }

            }
        }




        public static void sortStringArr3(String[] arr) {
            for (int j = 1; j < arr.length; j++) {
                String key = arr[j];
                int i = j - 1;
                while ((i > -1) && (arr[i].compareTo(key) > 0)) {
                    arr[i + 1] = arr[i];
                    i--;
                }
                arr[i + 1] = key;
            }
        }


        public static void main(String[] args) {
            String[] stringArr = {"Epam", "Abigail", "Necrobutcher", "Ban", "Lustra"};//bubble sort
            sortStringArr(stringArr);
            System.out.println(Arrays.toString(stringArr));
            String[] stringArr2 = {"Epam", "Alien", "Necrobutcher", "Sun", "Artur"};//selection sort
            sortStringArr(stringArr2);
            System.out.println(Arrays.toString(stringArr2));
            String[] stringArr3 = {"Epam", "Art", "Necrobutcher", "Doom", "Stol"};//insertion sort
            sortStringArr(stringArr3);
            System.out.println(Arrays.toString(stringArr3));
        }
    }


